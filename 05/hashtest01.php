<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>連想配列の基本的な書きかた・表示のさせ方</title>
  </head>
  <body>
    <h1>連想配列の基本的な書きかた・表示のさせ方</h1>
    <p>
        <?php
            $mydata = array(
            'fruit' => 'スイカ',
            'sport' => '野球',
            'town' => '横浜',
            'age' => 21,
            'food' => 'カレーライス'
            );
        ?>

      <pre>
          <?php var_dump($mydata); ?>
      </pre>

      <?php
          $mydata =
           [
          'fruit' => 'メロン',
          'sport' => 'テニス',
          'town' => '岡山',
          'age' => 29,
          'food' => 'ラーメン'
          ];
          // 検索する文字列
          $needle = "ラーメン";

          if(in_array($needle, $mydata))
          {
              echo $needle . " がmydataの要素の値に存在しています";
          }
          else
          {
              echo $needle . " がmydataの要素の値に存在しません";
          }
      ?>
      <pre>
          <?php var_dump($mydata); ?>
      </pre>
      <?php
        $mydata2['fruit'] = 'イチゴ';
        $mydata2['sport'] = 'サッカー';
        $mydata2['town'] = '北海道';
        $mydata2['age'] = 45;
        $mydata2['food'] = 'うどん';
      ?>
      <pre>
          <?php var_dump($mydata); ?>
      </pre>
    </p>
  </body>
</html>
