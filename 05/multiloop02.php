<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>データ構造の作成</title>
  </head>
  <body>
    <h1>データ構造の作成</h1>
    <p>
        <table border="1" style="">
           <?php
               $hunter01 = array
               (
                   "id" => "3",
                   "name" => "ゆうき",
                   "item" => "回復薬",
                   "buki" => "大剣",
                   "HR" => "21",
                );
               $hunter02 =  array
               (
                   "id" => "44",
                   "name" => "あああ",
                   "item" => "種",
                   "buki" => "双剣",
                   "HR" => "513",
                );
               $hunter03 =  array
               (
                   "id" => "2",
                   "name" => "かずき",
                   "item" => "粉塵",
                   "buki" => "ボウガン",
                   "HR" => "999",
                );

               $hunters = array($hunter01, $hunter02, $hunter03);

               foreach($hunters as $each)
               {
                   echo "<tr>";
                    echo  "<td>"."ID: " . $each['id'] ."</td>"
                        .  "<td>"."名前: " . $each['name'] .  "</td>"
                        .  "<td>"."アイテム: " . $each['item'] .  "</td>"
                        . "<td>". "武器: " . $each['buki'] . "</td>"
                        .  "<td>"."ハンターランク: " . $each['HR'] . "</td>";
                   echo "</tr>";
                }

           ?>
        </table>
    </p>
  </body>
</html>
