<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>二次元配列</title>
  </head>
  <body>
    <h1>二次元配列</h1>
    <p>
    <table border="1" style="">
        <?php
          $mysyumi1 = array("映画", "アニメ", "ゲーム", "散歩", "ラーメン");
          $mysyumi2 = array("目", "顔", "指", "手", "足");
          $mysyumi3 = array("時間", "バナナ", "イチゴ", "牛", "蛇");
          $mysyumi4 = array("明日", "港", "英語", "自慢", "機器");
          $syumi_all = array($mysyumi1, $mysyumi2, $mysyumi3, $mysyumi4);

          echo "<pre>";
          var_dump($syumi_all);
          echo "</pre>";

          ?>
    </table>
    </p>
  </body>
</html>
