<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>3. 行数と列数を指定してテーブルを書いてみよう</title>
  </head>
  <body>
    <h1>3. 行数と列数を指定してテーブルを書いてみよう</h1>
    <form method='get' action='loop03.php'>
        <p>
        <input type="number" name="tate" required>行×
        列<input type="number" name="yoko" required>
        <input type="submit">
        </form>
        <table border="1" style="">
          <?php
              for($i=1; $i <= $_GET['tate']; $i++)
              {
                 echo "<tr>";
                 for ($j=1; $j <= $_GET['yoko']; $j++)
                 {
                     echo "<td>" . "$i" . "-" . "$j" ."</td>";
                 }
                 echo "</tr>";
              }
          ?>
        </table>
        </p>
  </body>
</html>
